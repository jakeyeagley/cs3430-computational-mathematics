#!/usr/bin/python

#########################################
# module: img_ann.py
# YOUR NAME
# YOUR A-NUMBER
#########################################

import numpy as np
import pickle
import random as random

from img_ann_data import DATA, X, y, EVAL_DATA, EX, ey

A = np.full((1,25),.98123489814)
WHITE = np.array([0,1])
BLACK = np.array([1,0])
# sigmoid function you may want to use in training/evaluating
# your ANN.
def sigmoid(x, deriv=False):
    if (deriv == True):
        return x * (1 - x)
    return 1 / (1 + np.exp(-x))

def build_even_odd_nn():
    #two hidden layers
    np.random.randn(2, 2)
    W1 = np.random.rand(15,5)
    W2 = np.random.rand(5,2)
    W3 = np.random.rand(2,2)
    w4 = np.random.rand(2,2)

    #need to fill with random numbers
    return W1, W2, W3, W4

def build_nn_wmats(mat_dims):
    listOfMats = []
    inputMat = np.zeros((mat_dims[0],mat_dims[1]))
    outputMat = np.zeros((mat_dims[1],mat_dims[2]))
    for x in range(mat_dims[0]):
        for y in range(mat_dims[1]):
            inputMat[x,y] = np.random.uniform(-2,2)

    for x in range(mat_dims[1]):
        for y in range(mat_dims[2]):
            outputMat[x,y] = np.random.uniform(-2,2)

    listOfMats.append(inputMat)
    listOfMats.append(outputMat)

    return listOfMats

def bl():
    #two hidden layers
    np.random.randn(2, 2)
    W1 = np.random.rand(25,5)
    W2 = np.random.rand(5,2)
    W3 = np.random.rand(2,2)
    W4 = np.random.rand(2,2)

    #need to fill with random numbers
    return W1, W2, W3, W4

def train_5_layer_nn(numIters, X, y, build):
    W1, W2, W3, W4 = build()
    print 'W1'
    print W1
    #print 'W2'
    #print W2
    #print 'W3'
    #print W3
    for j in range(numIters):
        Z2 = np.dot(X,W1)
        #print 'Z2'
        #print Z2
        a2 = sigmoid(Z2)
        #print 'a2'
        #print a2
        Z3 = np.dot(a2,W2)
        #print 'Z3'
        #print Z3
        a3 = sigmoid(Z3)
        #print 'a3'
        #print a3
        Z4 = np.dot(a3,W3)
        #print 'Z4'
        #print Z4
        a4 = sigmoid(Z4)

        Z5 = np.dot(a4,W4)
        
        yHat = sigmoid(Z5)
        #print 'yHat'
        #print yHat
        yHat_error = y - yHat
        #print 'yHat_error'
        #print yHat_error
        yHat_delta = yHat_error * sigmoid(yHat, deriv=True)
        #print 'yHat_delta'
        #print yHat_delta
        a2_error = yHat_delta.dot(W2.T)
        #print 'a2_error'
        #print a2_error
        a2_delta = a2_error * sigmoid(a2, deriv=True)
        a3_error = yHat_delta.dot(W3.T)
        a3_delta = a3_error * sigmoid(a3, deriv=True)

        a4_error = yHat * sigmoid(a4, deriv=True)
        a4_delta = a4_error * sigmoid(a4, deriv=True)
        W4 += a4.T.dot(yHat_delta)
        W3 += a3.T.dot(a4_delta)
        W2 += a2.T.dot(a3_delta)
        W1 += X.T.dot(a2_delta)

    return W1, W2, W3

def fit_5_layer_nn(x, wmats, thresh=0.4, thresh_flag=False):
    input1 = sigmoid(np.dot(x, wmats[0]))
    output = sigmoid(np.dot(input1, wmats[1]))
    if thresh_flag == True:
        for x in np.nditer(output, op_flags=['readwrite']):
            if x > thresh:
                x[...] = 1
            else:
                x[...] = 0
        return output.astype(int)
    else:
        return output
    pass

def eval_img_nn(fit_fun, wmats, EX, ey):
    count = 0
    acc = 0
    for i in xrange(len(EX)):
        if np.array_equal(fit_fun(EX[i], wmats, thresh_flag=True),
                          ey[i]) == True:
            acc += 1
        count += 1
    return float(acc)/count

def find_best_nn(lower_num_iters, upper_num_iters, step, train_fun,
                 fit_fun, eval_fun, bn, X, y):
    for numIters in xrange(lower_num_iters, upper_num_iters, step):
        wmats = train_fun(numIters, X, y, bn)
        acc = eval_fun(fit_fun, wmats, X, y)
        print numIters, acc
        if acc > 0.8:
            return wmats
    return None
          
def pickle_nn(fp, wmats):
    pickle.dump(wmats, fp)

def unpickle_nn(fp):
    pickle.load(open(fp,'rb'))
    pass



    

    
