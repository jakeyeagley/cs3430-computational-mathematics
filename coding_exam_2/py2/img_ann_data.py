#!/usr/bin/python

#######################################################
# module: img_ann_data.py
# YOUR NAME
# YOUR A-NUMBER
########################################################

import cv2
import numpy as np
import os

# change these values accordingly.
img_black_dir = '/media/pi/Seagate Expansion Drive/cs3430/coding_exam_2/img_black'
img_white_dir = '/media/pi/Seagate Expansion Drive/cs3430/coding_exam_2/img_white'
img_eval_black_dir = '/media/pi/Seagate Expansion Drive/cs3430/coding_exam_2/img_eval_black'
img_eval_white_dir = '/media/pi/Seagate Expansion Drive/cs3430/coding_exam_2/img_eval_white'

## training and evaluation data
DATA = []
X, y = [], []
EVAL_DATA = []
EX, ey = [], []

def normalize_image(fp):
    img = cv2.imread(fp)
    img = img / 255
    img = np.array(img[0], dtype=float)
    r, c = img.shape
    img = img.reshape((1, r * c))
    return img
    

def create_data(img_dir, data_label):
    for path, dirlist, filelist in os.walk(img_dir):
        for f in filelist:
            tup = []
            tup.append(path +'/'+ f)
            tup.append(normalize_image(path +'/'+ f))
            tup.append(data_label)
            DATA.append(tup)

    np.random.shuffle(DATA)
    return DATA

def create_eval_data(img_dir, data_label):
    for path, dirlist, filelist in os.walk(img_dir):
        for f in filelist:
            tup = []
            tup.append(path +'/'+ f)
            tup.append(normalize_image(path +'/'+ f))
            tup.append(data_label)
            EVAL_DATA.append(tup)

    np.random.shuffle(EVAL_DATA)
    return EVAL_DATA

def create_Xy(DATA):
    global X, y
    for fp, img, yhat in DATA:
        X.append(img)
        y.append(yhat)
    X = np.array(X)
    y = np.array(y)

def create_EXey(EVAL_DATA):
    global EX, ey
    for fp, img, yhat in EVAL_DATA:
        EX.append(img)
        ey.append(yhat)
    EX = np.array(EX)
    ey = np.array(ey)
    
if __name__ == '__main__':
    create_data(img_black_dir, np.array([0, 1]))
    create_data(img_white_dir, np.array([1, 0]))
    np.random.shuffle(DATA)
    create_Xy(DATA)
    create_eval_data(img_eval_black_dir, np.array([0, 1]))
    create_eval_data(img_eval_white_dir, np.array([1, 0]))
    np.random.shuffle(EVAL_DATA)
    create_EXey(EVAL_DATA)











