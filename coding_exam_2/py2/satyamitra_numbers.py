#!/usr/bin/python

###################################
# module: satyamitra_numbers.py
# Jake Yeagley
# A01503588
###################################

import numpy as np
import math

#online help sources that I used

#get the divisor of a number
#https://stackoverflow.com/questions/171765/what-is-the-best-way-to-get-all-the-divisors-of-a-number
#how to find perfect square
#https://stackoverflow.com/questions/2489435/how-could-i-check-if-a-number-is-a-perfect-square
#a variety of numpy functions
#https://www.tutorialspoint.com/numpy

# function that computes the list of proper
# factors of n.
def proper_factors(n):
    factors = []
    for i in range(1, (n/2) + 1):
        if n % i == 0:
            factors.append(i)

    return factors

# function that computes the determinant of
# 2d numpy array
def minorMat(mat, i, j):
    mat = np.delete(mat, (i), axis=0)
    mat = np.delete(mat, (j), axis=1)

    return mat

def det(mat):
    i, j = mat.shape
    if(i == 1):
        return mat[0,0]
    elif(i == 2):
        return mat[0,0] * mat[1,1] - mat[0,1] * mat[1,0]
    else:
        values = []
        for index in range(j):
            values.append(mat[0,index] * (-1)**(index) * det(minorMat(mat,0,index)))
        return sum(values)

# function that computes the list of pairs
# of satyamitra numbers in [lower, upper].
def satyamitra_numbers_in_range(lower, upper):
    pair = []
    for u in range(lower, upper + 1):
        #print 'u: ', u
        for l in range(lower, upper + 1):
            #print 'l: ', l
            #print 'sum of factors of ',l, ' = ', sum(proper_factors(l))
            #print 'sum of factors of ',u, ' = ', sum(proper_factors(u))
            if(sum(proper_factors(l)) == u and [u,l] not in pair and [l,u] not in pair):
                #print 'pair: ', l, ' and ', u
                pair.append([l,u])
            if(sum(proper_factors(u)) == l and [l,u] not in pair and [u,l] not in pair):
                #print 'pair: ', u, ' and ' , l
                pair.append([u,l])

    return pair

# function that computes creates an n x n
# matrix from the list of satyamitra number
# pairs.
def satyamitra_matrix(sn_list):
    #check if number is square
    print 'length: ', len(sn_list)
    print 'not int: ', math.sqrt(len(sn_list) * 2)
    print 'int: ', int(math.sqrt(len(sn_list) * 2))
    if(len(sn_list) == 2):
        return np.array(sn_list)
    if(math.sqrt(len(sn_list) * 2) != int(math.sqrt(len(sn_list) * 2))):
        return 'not square'

    #create matrix of size of the square root of sn_list
    #mat = np.zeros((int(math.sqrt(len(sn_list) * 2)),int(math.sqrt(len(sn_list) * 2))))

    
    mat = np.array(sn_list)
    mat = mat.reshape((int(math.sqrt(len(sn_list) * 2)),int(math.sqrt(len(sn_list) * 2))))
    return mat
    
    


                



