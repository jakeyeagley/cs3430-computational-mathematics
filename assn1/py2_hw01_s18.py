#!/usr/bin/python

#########################################
## CS 3430: S2018: HW01: Euclid Numbers
## Jake Yeagley
## A01503588
#########################################

import math
from numpy import prod

def is_prime(n):
    '''is_prime(n) ---> True if n is prime; False otherwise.'''
    # your code here
    for i in range(2, n):
        if n % i == 0:
            return False
    return True

def next_prime_after(p):
    '''computes next prime after prime p; if p is not prime, returns None.'''
    if not is_prime(p): return None

    nextPrime = False
    while not nextPrime:
        p = p + 1
        nextPrime = is_prime(p)
    return p    

def euclid_number(i):
    '''euclid_number(i) --> i-th Euclid number.'''
    if i < 0: return None

    list_of_primes = [2]
    prime = 2
    for x in range(i):
        list_of_primes.append(next_prime_after(prime))
        prime = next_prime_after(prime)
        
    return prod(list_of_primes) + 1

def compute_first_n_eucs(n):
    '''returns a list of the first n euclid numbers.'''
    eucs = []

    for x in range(n):
        eucs.append(euclid_number(x))

    return eucs

def prime_factors_of(n):
    '''returns a list of prime factors of n if n > 1 and [n] otherwise.'''
    if n < 2: return []
    factors = []

    #is the number prime?
    if is_prime(n):
        factors.append(n)
        return factors

    #https://stackoverflow.com/questions/16996217/prime-factorization-list
    i = 2
    while i * i <= n:
        while (n % i) == 0:
            factors.append(i)
            n //= i
        i = i + 1
    if n > 1:
        factors.append(n)
        
    return factors

def tabulate_euc_factors(n):
    '''returns a list of 3-tuples (i, euc, factors).'''
    euc_factors = []

    for i in range(n + 1):
        tef = []
        tef.append(i)
        tef.append(euclid_number(i))
        tef.append(prime_factors_of(euclid_number(i)))
        euc_factors.append(tef)
        
    return euc_factors

def pair(x, y):
    '''returns z such that <x, y> = z.'''
    return (((2 * y) + 1) * 2**x) - 1

def find_pair_of(z):
    '''returns the tuple (x, y) such that <x, y> = z.''' 
    right = right_of(z)
    left = left_of(z)
    tup = (left,right)
    return tup

def left_of(z):
    '''returns x such that <x, y> = z.'''
    z = z + 1
    for i in range(z):
        if z % 2**i == 0:
            x = i
    return x

def right_of(z):
    '''returns y such that <x, y> = z.'''
    x = left_of(z)
    z = z + 1
    y = z / 2**x
    return (y - 1) / 2 






                     

    

