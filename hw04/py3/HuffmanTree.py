#!/usr/bin/python

######################################
## module: HuffmanTreeNode.py
## Jake Yeagley
## A01503588
######################################

from HuffmanTreeNode import HuffmanTreeNode
    
class HuffmanTree(object):
    def __init__(self, root=None):
        self.__root = root
        self.__dict = {}

    def getRoot(self):
        return self.__root

    def setDict(self, key, value):
        temp = {key : value}
        self.__dict.update(temp)

    def checkNode(self, HTN, code, s):
        if HTN.isLeaf():
            return code
        elif s in HTN.getLeftChild().getSymbols():
            code = code + "0"
            code = self.checkNode(HTN.getLeftChild(), code, s)
        elif s in HTN.getRightChild().getSymbols():
            code = code + "1"
            code = self.checkNode(HTN.getRightChild(), code, s)

        return code
    
    def encodeSymbol(self, s):
        code = ""
        if not s in self.__root.getSymbols():
            raise Exception('Unknown symbol')
        else:
            if(s in self.getRoot().getLeftChild().getSymbols()):
                code = "0"
                code = self.checkNode(self.getRoot().getLeftChild(), code, s)
            elif(s in self.getRoot().getRightChild().getSymbols()):
                code = "1"
                code = self.checkNode(self.getRoot().getRightChild(), code, s)
            
        return code

    def encodeText(self, txt):
        ans = ""
        for letter in txt:
            encode = self.encodeSymbol(letter)
            self.setDict(encode,letter)
            ans = ans + encode

        return ans
    
#https://stackoverflow.com/questions/33089660/decoding-a-huffman-code-with-a-dictionary
    def decode(self, bin_string):
        code = ""
        while len(bin_string) != 0:
            for key in self.__dict:
                if bin_string.startswith(key):
                    code = code + self.__dict[key]
                    bin_string = bin_string[len(key):]

        return code
    
    @staticmethod
    def mergeTwoNodes(htn1, htn2):
        #print 'Merging', str(htn1), str(htn2)
        symbols = set(htn1.getSymbols())
        for i in htn2.getSymbols():
            symbols.add(i)
        n = HuffmanTreeNode(symbols=symbols, weight=htn1.getWeight() + htn2.getWeight())
        n.setLeftChild(htn1)
        n.setRightChild(htn2)
        return n

    @staticmethod
    def findTwoLowestWeightNodes(list_of_nodes):
        list_of_nodes.sort(key=lambda n: n.getWeight())
        return list_of_nodes[0], list_of_nodes[1]

    @staticmethod
    def displayListOfNodes(list_of_nodes):
        for n in list_of_nodes:
            print(str(n))

    @staticmethod
    def fromListOfHuffmanTreeNodes(list_of_nodes):
        # find lowest two 
        # new node root = merge n1 and n2 into a new node:
        # remove n1 and n2 from list_of_nodes
        # add the merged node back to list_of_nodes
        #while len(list_nodes) > 1
        #n1 n2 find lowest weight
        #remove
        #remove
        #append

        if len(list_of_nodes) == 0:
            raise Exception('Cannot construct from empty list of nodes')
        elif len(list_of_nodes) == 1:
            return HuffmanTree(root=list_of_nodes[0])
        else:
            while len(list_of_nodes) > 1:
                n1, n2 = HuffmanTree.findTwoLowestWeightNodes(list_of_nodes)
                list_of_nodes.remove(n1)
                list_of_nodes.remove(n2)
                list_of_nodes.append(HuffmanTree.mergeTwoNodes(n1,n2))

            HT = HuffmanTree(list_of_nodes[0])
            return HT

    @staticmethod
    def freqMapToListOfHuffmanTreeNodes(freq_map):
        return [HuffmanTreeNode(symbols=set([item[0]]), weight=item[1]) for item in freq_map.items()] 


    
