#!/usr/bin/python

#################################
# module: encode_dna_strings.py
# YOUR NAME
# YOUR A-NUMBER
#################################

import re
import sys
import os
import fnmatch

from HuffmanTree import HuffmanTree
from HuffmanTreeNode import HuffmanTreeNode
from BinHuffmanTree import BinHuffmanTree
from CharFreqMap import CharFreqMap

def generate_file_names(fnpat, rootdir):

  file_list = []
  x = 0
  for path, dirlist, filelist in os.walk(rootdir):
    for file_name in fnmatch.filter(filelist, fnpat):
      file_list.append(os.path.splitext(file_name)[0])

  return file_list

def generate_input_streams(gen_filenames):
  tup = []
  count = 0

  for x in gen_filenames:
    filePath = open(x,'w')
    temp = []
    temp.append(x)
    temp.append(filePath)
    tup.append(temp)

  return tup

def generate_dna_strings(gen_instreams):
  tup = []
  for stream in gen_instreams:
    temp = []
    temp.append(stream[0])
    x = ""
    for n in sorted([line for line in stream[1].readlines()]):
      x = x + n
    temp.append(x)
    tup.append(temp)

  return tup

def encode_dna_strings(fnpat, rootdir, encdir):
  fns = generate_file_names(fnpat, rootdir)
  for fn in fns:
    print "encoding ", encdir + fn
    cfm1 = CharFreqMap.computeCharFreqMap(rootdir + fn + '.txt')
    nodes = HuffmanTree.freqMapToListOfHuffmanTreeNodes(cfm1)
    ht = HuffmanTree.fromListOfHuffmanTreeNodes(nodes)
    bht = BinHuffmanTree(root=ht.getRoot())
    bht.encodeTextFromFileToFile(rootdir + fn + '.txt', encdir + fn)
    
  pass
    
if __name__ == '__main__':
  encode_dna_strings(sys.argv[1], sys.argv[2], sys.argv[3])














