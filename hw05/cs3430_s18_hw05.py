#!/usr/bin/python

##########################################
## module: cs3430_s18_hw05.py
## Jake Yeagley
## A01503588
##########################################

import numpy as np

#from sympy import *
#matrix.rref()

def getElementsOfAColumn(m,rowSize,colNum):
    "returns a column number colNumn, from matrix m of size size"
    list = []
    for row in range(rowSize):
        list.append(m[row,colNum])

    matrix = np.array(list)
    return matrix

def findZerosAndSwitchRows(m,rowNum,nonZeroCol):
    rowOneFound = False

    for row in range(rowNum):
        if(nonZeroCol[row] != 0 and rowOneFound == True):
           r2 = row
           m[[r1,r2]] = m[[r2,r1]]
           rowOneFound = False
        if(nonZeroCol[row] == 0 and rowOneFound == False):
            if(row != rowNum - 1):
                r1 = row
                rowOneFound = True

    return m

def reduceCol(m):
    size, n = m.shape
    
    for row in range(size - 1):
        r = -m[row + 1,0]
        if(r == 0):
            r = -m[row + 1,1]

        m[row + 1,:] = r * m[0,:] + m[row + 1,:]

    return m

def reduceRow(m):
    rowSize, size = m.shape
    if np.count_nonzero(m) == 0:
        return m
    if(size - 1 >= 2):
        if(m[1,1] == 0 and m[1,2] == 0):
            return m

    if(m[1,1] == 0 and size - 1 >= 2):
        r = -m[0,2]
    elif(m[1,1] == 0 and size - 1 < 2):
        return m
    else:
        r = -m[0,1]
        
    m[0,:] = (m[1,:] * r) + m[0,:] 
    
    return m
    
def reduceToREF(m):
    rowSize,colSize =  m.shape
    
    nonZeroColIndex = 0
    for col in range(colSize):
        nonZeroCol = getElementsOfAColumn(m,rowSize,col)
        if(np.sum(abs(nonZeroCol)) != 0):
            nonZeroColIndex = col
            break

    m = findZerosAndSwitchRows(m,rowSize,nonZeroCol)

    col = 0
    for row in range(rowSize):
        p = m[row,nonZeroColIndex]
        if(nonZeroColIndex + 1 == colSize):
            continue
        if(p == 0):
            nonZeroColIndex = nonZeroColIndex + 1
            p = m[row,nonZeroColIndex]
        else:
            m[row,:] = ((1/p) * m[row,:])
        m[row:rowSize,row:colSize] = reduceCol(m[row:rowSize,row:colSize])
        nonZeroColIndex = nonZeroColIndex + 1

    for row in range(rowSize - 1):
        m[row:rowSize,row:colSize] = reduceRow(m[row:rowSize,row:colSize])

    if(2 < rowSize):
        if(m[0,2] != 0):
            r = -m[0,2]
            m[0,3] = r + m[0,3]
            m[0,2] = r + m[0,2]
        
    return m

def solveLinSys(m):
    canSolve = True
    
    m = reduceToREF(m)
    rowSize, colSize = m.shape
    squareMatrix = np.delete(m,np.s_[rowSize,colSize - 1],axis=1)
    ans = m[:,colSize - 1]

    #solved = np.linalg.solve(squareMatrix, ans)
    #try:
    #   solved = mp.linalg.solve(squareMatrix, ans)
    #except np.linAlgError:
    #    return False
    #else:
    #   return True
    print squareMatrix
    print ans
    print m

    print ans[0]
    for row in range(rowSize):
        if(ans[row] == 0):
            #check square matrix
            for col in range(colSize - 1):
                if(squareMatrix[row,col] != 0):
                    return False
        else:
            #check square matrix
            temp = []
            for col in range(colSize - 1):
                temp.append(squareMatrix[row,col])
            if(all([ele == 0 for ele in temp])):
                return False
    
    return True

def invertMat(m):
    try:
        inverse = np.linalg.inv(m)
    except np.linalg.LinAlgError:
        return None
    else:
        return inverse


        








            
    


    

