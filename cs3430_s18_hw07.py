#!/usr/bin/python

#####################################
# module: cs3430_s18_hw07.py
# Jake Yeagley
# A01503588
#####################################

import numpy as np
import pickle as cPickle
import random as random

#from cs3430_s18_hw07_data import *

# sigmoid function
def sigmoid(x, deriv=False):
    if (deriv == True):
        return x * (1 - x)
    return 1 / (1 + np.exp(-x))

def build_nn_wmats(mat_dims):
    listOfMats = []
    inputMat = np.zeros((mat_dims[0],mat_dims[1]))
    outputMat = np.zeros((mat_dims[1],mat_dims[2]))
    for x in range(mat_dims[0]):
        for y in range(mat_dims[1]):
            inputMat[x,y] = np.random.uniform(-2,2)

    for x in range(mat_dims[1]):
        for y in range(mat_dims[2]):
            outputMat[x,y] = np.random.uniform(-2,2)

    listOfMats.append(inputMat)
    listOfMats.append(outputMat)

    return listOfMats

def build_231_nn():
    return build_nn_wmats((2, 3, 1))

def build_838_nn():
    return build_nn_wmats((8, 3, 8))

def build_even_odd_nn():
    #two hidden layers
    np.random.randn(2, 2)
    W1 = np.random.rand(8,6)
    W2 = np.random.rand(6,2)
    W3 = np.random.rand(2,2)

    #need to fill with random numbers
    return W1, W2, W3
                      
def create_nn_data():
    inputMat = np.zeros((10,8))
    outputMat = np.zeros((10,2))

    inputMat[1,7] = 1
    inputMat[2,6] = 1
    inputMat[3,7] = 1
    inputMat[3,6] = 1
    inputMat[4,5] = 1
    inputMat[5,7] = 1
    inputMat[5,5] = 1
    inputMat[6,6] = 1
    inputMat[6,5] = 1
    inputMat[7,7] = 1
    inputMat[7,6] = 1
    inputMat[7,5] = 1
    inputMat[8,4] = 1
    inputMat[9,7] = 1
    inputMat[9,4] = 1

    outputMat[0,0] = 1
    outputMat[1,1] = 1
    outputMat[2,0] = 1
    outputMat[3,1] = 1
    outputMat[4,0] = 1
    outputMat[5,1] = 1
    outputMat[6,0] = 1
    outputMat[7,1] = 1
    outputMat[8,0] = 1
    outputMat[9,1] = 1

    return inputMat, outputMat

def train_4_layer_nn(numIters, X, y, build):
    W1, W2, W3 = build()
    #print 'W1'
    #print W1
    #print 'W2'
    #print W2
    #print 'W3'
    #print W3
    for j in range(numIters):
        Z2 = np.dot(X,W1)
        #print 'Z2'
        #print Z2
        a2 = sigmoid(Z2)
        #print 'a2'
        #print a2
        Z3 = np.dot(a2,W2)
        #print 'Z3'
        #print Z3
        a3 = sigmoid(Z3)
        #print 'a3'
        #print a3
        Z4 = np.dot(a3,W3)
        #print 'Z4'
        #print Z4
        yHat = sigmoid(Z4)
        #print 'yHat'
        #print yHat
        yHat_error = y - yHat
        #print 'yHat_error'
        #print yHat_error
        yHat_delta = yHat_error * sigmoid(yHat, deriv=True)
        #print 'yHat_delta'
        #print yHat_delta
        a2_error = yHat_delta.dot(W2.T)
        #print 'a2_error'
        #print a2_error
        a2_delta = a2_error * sigmoid(a2, deriv=True)
        a3_error = yHat_delta.dot(W3.T)
        a3_delta = a3_error * sigmoid(a3, deriv=True)
        W3 += a3.T.dot(yHat_delta)
        W2 += a2.T.dot(a3_delta)
        W1 += X.T.dot(a2_delta)

    return W1, W2, W3

def fit_4_layer_nn(x, wmats, thresh=0.4, thresh_flag=False):
    #print 'x'
    #print x
    #print 'wmats'
    #print wmats
    input1 = sigmoid(np.dot(x, wmats[0]))
    output = sigmoid(np.dot(input1, wmats[1]))
    if thresh_flag == True:
        for x in np.nditer(output, op_flags=['readwrite']):
            if x > thresh:
                x[...] = 1
            else:
                x[...] = 0
        return output.astype(int)
    else:
        return output
    pass


def is_even_nn(n, wmats):
    #https://stackoverflow.com/questions/699866/python-int-to-binary
    binNum = "{0:{fill}8b}".format(n, fill='0')
    binNum = list(binNum)
    for i in range(len(binNum)):
        binNum[i] = float(binNum[i])

    fitMat = fit_4_layer_nn(binNum, wmats)
    #print 'fitMat'
    #print fitMat
    if(fitMat[0] > .9):
        return True
    else:
        return False

def eval_even_odd_nn(wmats):
    trueListNN = []
    trueListMod = []
    for i in range(128):
        if(is_even_nn(i,wmats)):
            trueListNN.append(1)
        else:
            trueListNN.append(0)
        if(i % 2 == 0):
            trueListMod.append(1)
        else:
            trueListMod.append(0)

    return float(sum(trueListNN)) / float(sum(trueListMod))

def train_3_layer_nn(numIters, X, y, build):
    W1, W2 = build()
    #print 'W1'
    #print W1
    #print 'W2'
    #print W2
    for j in range(numIters):
        Z2 = np.dot(X, W1)
        #print 'Z2'
        #print Z2
        a2 = sigmoid(Z2)
        #print 'a2'
        #print a2
        Z3 = np.dot(a2, W2)
        #print 'Z3'
        #print Z3
        yHat = sigmoid(Z3)
        #print 'yHat'
        #print yHat
        yHat_error = y - yHat
        #print 'yHat_error'
        #print yHat_error
        yHat_delta = yHat_error * sigmoid(yHat, deriv=True)
        #print 'yHat_delta'
        #print yHat_delta
        a2_error = yHat_delta.dot(W2.T)
        #print 'a2_error'
        #print a2_error
        a2_delta = a2_error * sigmoid(a2, deriv=True)
        #print 'a2_delta'
        #print a2_delta
        W2 += a2.T.dot(yHat_delta)
        #print 'W2'
        #print W2
        W1 += X.T.dot(a2_delta)
        #print 'W1'
        #print W1

    return W1, W2

def fit_3_layer_nn(input0, weightMat, thresh=0.4, thresh_flag=True):
    input1 = sigmoid(np.dot(input0, weightMat[0]))
    output = sigmoid(np.dot(input1, weightMat[1]))
    if thresh_flag == True:
        for x in np.nditer(output, op_flags=['readwrite']):
            if x > thresh:
                x[...] = 1
            else:
                x[...] = 0
        return output.astype(int)
    else:
        return output
