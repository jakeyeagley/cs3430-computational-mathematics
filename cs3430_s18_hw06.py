#/usr/bin/python

##########################################
# module: cs3430_s18_hw06.py
# Jake Yeagley
# A01503588
##########################################

import numpy as np

def minorMat(mat, i, j):
    mat = np.delete(mat, (i), axis=0)
    mat = np.delete(mat, (j), axis=1)

    return mat

def det(mat):
    i, j = mat.shape
    if(i == 1):
        return mat[0,0]
    elif(i == 2):
        return mat[0,0] * mat[1,1] - mat[0,1] * mat[1,0]
    else:
        values = []
        for index in range(j):
            values.append(mat[0,index] * (-1)**(index) * det(minorMat(mat,0,index)))
        return sum(values)

    #return np.linalg.det(mat)

def cofactor(mat, i, j):
    return round((-1)**(i + j) * det(minorMat(mat,i,j)),2)

def expandByRowMinors(mat, r):
    i,j = mat.shape
    sums = []
    for x in range(i):
        sums.append(mat[x,j - 1] * cofactor(mat,x,j - 1))

    return sum(sums)

def expandByColMinors(mat, c):
    i,j = mat.shape
    sums = []
    for x in range(j):
        sums.append(mat[i - 1,x] * cofactor(mat,i - 1,x))

    return sum(sums)

def cofactorMat(mat):
    i, j = mat.shape

    tempMat = np.zeros((i,j), dtype=float)
    for x in range(i):
        for y in range(j):
            tempMat[x,y] = cofactor(mat, x, y)

    return tempMat

def adjointMat(mat):
    coMat = cofactorMat(mat)
    return coMat.transpose()

def inverseMat(mat):
    return (1 / det(mat)) * adjointMat(mat)

def cramer(A, b):
    i, j = A.shape
    C = np.zeros((i,j), dtype=float)
    C = np.copy(A)
    ans = []
    for x in range(len(b)):
        for y in range(len(b)):
            C[y][x] = b[y]

        ans.append(round(det(C) / det(A),10))
        C = np.copy(A)
    return ans
    
def checkInverse(A):
    D = det(A)
    print(D)
    if D != 0.0:
        print(inverseMat(A))
        print(np.dot(A, inverseMat(A)))
        
