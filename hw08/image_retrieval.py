#!/usr/bin/python

import argparse
import cv2
import sys
import os
import re
import cPickle as pickle
import math
from scipy import spatial
import operator

########################
# module: image_retrieval.py
# Jake Yeagley
# A01503588
########################

##Note: retrieval works better with less pictures

ap = argparse.ArgumentParser()
ap.add_argument('-i', '--imgpath', required = True, help = 'image path')
ap.add_argument('-bgr', '--bgr', required = True, help = 'bgr index file to unpickle')
ap.add_argument('-hsv', '--hsv', required = True, help = 'hsv index file to unpickle')
ap.add_argument('-gsl', '--gsl', required = True, help = 'gsl index file to unpickle')
args = vars(ap.parse_args())

def dot_product(v1, v2):
  return sum(map(operator.mul, v1,v2))

def mean(v):
  return sum(v)/(len(v)*1.0)

# compute the bgr similarity between
# two bgr index vectors
def bgr_img_sim(img_index_vec1, img_index_vec2):
  prod = dot_product(img_index_vec1, img_index_vec2)
  len1 = math.sqrt(dot_product(img_index_vec1,img_index_vec1))
  len2 = math.sqrt(dot_product(img_index_vec2,img_index_vec2))
  return prod / (len1 * len2) 
  
# compute the hsv similarity between
# two hsv index vectors
def hsv_img_sim(img_index_vec1, img_index_vec2):
  return 1 - spatial.distance.cosine(img_index_vec1, img_index_vec2)

# compute the hsv similarity between
# two gsl index vectors
def gsl_img_sim(img_index1, img_index2):
  return 1 - spatial.distance.cosine(img_index1, img_index2)

# index the input image
def index_img(imgp):
    try:
        img = cv2.imread(imgp)
        if img is None:
          print('cannot read ' + imgp)
          return
        rslt = (index_bgr(img), index_hsv(img), index_gsl(img))
        del img
        return rslt
    except Exception, e:
        print(str(e))

# this is very similar to index_bgr in image_index.py except
# you do not have to save the index in BGR_INDEX. This index
# is used to match the indices in the unpickeld BGR_INDEX.
def index_bgr(img):
    bgri = []
    red, blue, green = cv2.split(img)

    for i in range(len(img)):
      #print 'mean(red[i]): ', mean(red[i])
      bgri.append([mean(red[i]), mean(blue[i]), mean(green[i])])
      
    return bgri

# this is very similar to index_hsv in image_index.py except
# you do not have to save the index in HSV_INDEX. This index
# is used to match the indices in the unpickeld HSV_INDEX.
def index_hsv(img):
    hsvi = []
    hue, sat, value = cv2.split(img)
    
    for i in range(len(img)):
      hsvi.append([mean(hue[i]), mean(sat[i]), mean(value[i])])

    return hsvi

# this is very similar to index_gs. in image_index.py except
# you do not have to save the index in GSL_INDEX. This index
# is used to match the indices in the unpickeld GSL_INDEX.
def index_gsl(img):
    #print 'gsli_index'
    gsli = []
    gray_scale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.split(gray_scale)

    for i in range(len(gray_scale)):
      gsli.append([mean(gray[0][i])])

    #print 'gsli: ', gsli
    return gsli

# we will unpickle into these global vars below.
BGR_INDEX = None
HSV_INDEX = None
GSL_INDEX = None

# compute the similarities between the bgr
# index vector and all the vectors in the unpickled
# bgr index bgr_index and return the top one.
def compute_bgr_sim(bgr, bgr_index, topn=1):
  # your code
  scores = {}
  blue = []
  red = []
  green = []
  blueI = []
  redI = []
  greenI = []

  for path, img in bgr_index.iteritems():
    print 'comuting bgr of ', path
    for i in range(len(img)):
      blue.append(bgr[i][0])
      red.append(bgr[i][1])
      green.append(bgr[i][2])
      blueI.append(img[i][0])
      redI.append(img[i][1])
      greenI.append(img[i][2])
    
    #print 'i: ', i
    blue_sim = bgr_img_sim(blue,blueI)
    #print 'bgr[i][0]: ', bgr[i][0]
    red_sim = bgr_img_sim(red,redI)
    green_sim = bgr_img_sim(green,greenI)
  
    #print 'blue_sim for ', path, ': ', blue_sim
    #print 'red_sim: ', red_sim
    #print 'green_sim: ', green_sim
    score = (blue_sim + red_sim + green_sim) / 3
    scores.update({path : score})

  #print 'bgr_score: ', scores
  max_value = max(scores.iteritems(), key=operator.itemgetter(1))[0]
  
  return scores[max_value], max_value

# compute the similarities between the hsv
# index vector and all the vectors in the unpickled
# hsv index hsv_index and return the top one.
def compute_hsv_sim(hsv, hsv_index, topn=1):
    # your code
  scores = {}
  hue = []
  sat = []
  val = []
  hueI = []
  satI = []
  valI = []

  for path, img in hsv_index.iteritems():
    print 'comuting hsv of ', path
    for i in range(len(img)):
      hue.append(hsv[i][0])
      sat.append(hsv[i][1])
      val.append(hsv[i][2])
      hueI.append(img[i][0])
      satI.append(img[i][1])
      valI.append(img[i][2])
    
    #print 'i: ', i
    hue_sim = hsv_img_sim(hue,hueI)
    #print 'bgr[i][0]: ', bgr[i][0]
    sat_sim = hsv_img_sim(sat,satI)
    val_sim = hsv_img_sim(val,valI)
    
    score = (hue_sim + sat_sim + val_sim) / 3
    scores.update({path : score})

  #print 'hsv_score: ', scores
  max_value = max(scores.iteritems(), key=operator.itemgetter(1))[0]
  
  return scores[max_value], max_value

# compute the similarities between the gsl
# index vector and all the vectors in the unpickled
# gsl index gls_index and return the top one.
def compute_gsl_sim(gsl, gsl_index, topn=1):
  scores = {}
  gray = []
  grayI = []

  #print 'gsl: ', gsl
  for path, img in gsl_index.iteritems():
    print 'computing gsl of ', path
    for i in range(len(img)):
      gray.append(gsl[i][0])
      grayI.append(img[i][0])

    #print len(gray)
    #print len(grayI)
    gray_sim = gsl_img_sim(gray,grayI)
    scores.update({path : gray_sim})

  max_value = max(scores.iteritems(), key=operator.itemgetter(1))[0]
  #print 'gsl_score: ', scores
  
  return scores[max_value], max_value

# unpickle, match, and display
if __name__ == '__main__':
  with open(args['bgr'], 'rb') as bgrfile:
    BGR_INDEX = pickle.load(bgrfile)
  with open(args['hsv'], 'rb') as hsvfile:
    HSV_INDEX = pickle.load(hsvfile)
  with open(args['gsl'], 'rb') as gslfile:
    GSL_INDEX = pickle.load(gslfile)

  bgr, hsv, gsl = index_img(args['imgpath'])
  bgr_matches = compute_bgr_sim(bgr, BGR_INDEX)
  hsv_matches = compute_hsv_sim(hsv, HSV_INDEX)
  gsl_matches = compute_gsl_sim(gsl, GSL_INDEX)
  
  print 'bgr_matches: ', bgr_matches
  print 'hsv_matches: ', hsv_matches
  print 'gls_matches: ', gsl_matches

  orig = cv2.imread(args['imgpath'])
  bgr = cv2.imread(bgr_matches[1])
  hsv = cv2.imread(hsv_matches[1])
  gsl = cv2.imread(hsv_matches[1])
  cv2.imshow('Input', orig)
  cv2.imshow('BGR', bgr)
  cv2.imshow('HSV', hsv)
  cv2.imshow('GSL', gsl)
  cv2.waitKey()
  del orig
  del bgr
  del hsv
  del gsl
  cv2.destroyAllWindows()
    

