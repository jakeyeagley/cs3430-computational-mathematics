#!/usr/bin/python

import argparse
import cv2
import sys
import os
import re
import cPickle as pickle
import fnmatch

########################
# module: image_index.py
# Jake Yeagley
# A01503588
########################

ap = argparse.ArgumentParser()
ap.add_argument('-imgdir', '--imgdir', required = True, help = 'image directory')
ap.add_argument('-bgr', '--bgr', required = True, help = 'bgr index file to pickle')
ap.add_argument('-hsv', '--hsv', required = True, help = 'hsv index file to pickle')
ap.add_argument('-gsl', '--gsl', required = True, help = 'gsl index file to pickle')
args = vars(ap.parse_args())

def generate_file_names(fnpat, rootdir):

  file_list = []
  for path, dirlist, filelist in os.walk(rootdir):
    for file_name in fnmatch.filter(filelist, fnpat):
      #print 'file name: ', file_name
      file_list.append(os.path.join(path, file_name))
      
  return file_list
  
## three index dictionaries
HSV_INDEX = {}
BGR_INDEX = {}
GSL_INDEX = {}

def mean(v):
  return sum(v)/(len(v)*1.0)
  
def index_img(imgp):
    try:
        img = cv2.imread(imgp)
        index_bgr(imgp, img)
        index_hsv(imgp, img)
        index_gsl(imgp, img)
        del img
    except Exception, e:
        print(str(e))


# compute the bgr vector for img saved in path imgp and
# index it in BGR_INDEX under imgp.
def index_bgr(imgp, img):
    bgri = []
    red, blue, green = cv2.split(img)
    #print 'img: ', img
    #print 'red: ', red
    
    for i in range(len(img)):
      bgri.append([mean(red[i]), mean(blue[i]), mean(green[i])])

    #print 'bgri: ', bgri
    BGR_INDEX[imgp] = bgri
    #print 'BGR_INDEX: ', BGR_INDEX
    pass

# compute the hsv vector for img saved in path imgp and
# index it in HSV_INDEX under imgp.
def index_hsv(imgp, img):
    hsvi = []
    hue, sat, value = cv2.split(img)
    for i in range(len(img)):
      hsvi.append([mean(hue[i]), mean(sat[i]), mean(value[i])])

    HSV_INDEX[imgp] = hsvi
    pass
  
# compute the gsl vector for img saved in path imgp and
# index it in GSL_INDEX under imgp.
def index_gsl(imgp, img):
  gsli = []
  gray_scale = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  gray = cv2.split(gray_scale)
  for i in range(len(img)):
    gsli.append([mean(gray[0][i])])
  GSL_INDEX[imgp] = gsli
  pass

# index image directory imgdir
def index_img_dir(imgdir):
  print imgdir
  for imgp in generate_file_names(r'*_orig.png', imgdir):
    print('indexing ' + imgp)
    index_img(imgp)
    print(imgp + ' indexed')

# index and pickle
if __name__ == '__main__':
  index_img_dir(args['imgdir'])
  with open(args['bgr'], 'wb') as bgrfile:
    pickle.dump(BGR_INDEX, bgrfile)
  with open(args['hsv'], 'wb') as hsvfile:
    pickle.dump(HSV_INDEX, hsvfile)
  with open(args['gsl'], 'wb') as gslfile:
    pickle.dump(GSL_INDEX, gslfile)
  print('indexing finished')
    

