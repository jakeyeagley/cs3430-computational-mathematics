
#/usr/bin/python

#########################################
## CS 3430: S2018: HW02: DNA String Analysis
## Jake Yeagley
## A01503588
#########################################

from dna_data import *

import re

pat00 = r'(CC)'
pat01 = r'(T{3,5})'
pat02 = r'(T{3,5})(A|G)'
pat03 = r'(A|G)(C{2,5}|T{3,10})(G?)'
pat04 = r'(A|T)(G{5,15}|C{2,11})(AA|GGG)'

pat05 = r'(AAAA+)'
pat06 = r'(GGGG+)'
pat07 = r'(CCCC+)'
pat08 = r'(TTTT+)'

pat09 = r'(G{2,5})(A+)(C)(G{2,5})'
pat10 = r'(C{2,5})(A+)(T)(C{2,5})'
pat11 = r'(T{2,5})(A+)(G)(T{2,5})' 

def find_pat_matches(pat, txt):
    ## your code
    ans = []

    it = re.finditer(pat, txt)

    for i in it:
        element = []
        span = []
        
        element.append(i.group(0))
        span.append(i.span())

        print span[0][0]
        element.append(span[0][0])
        print span[0][1]
        element.append(span[0][1])
        ans.append(element)
        
    return ans

def sort_pat_matches_by_span(pat, txt, reverse=True):
    ## your code
    tup = find_pat_matches(pat,txt)

    if reverse:
        tup.sort(key=lambda x: len(x[0]))
        tup.reverse()

    else:
        tup.sort(key=lambda x: len(x[0]))
    
    return tup

def top_n_pat_matches_by_span(pat, txt, n):
    tup = sort_pat_matches_by_span(pat, txt, reverse=True)

    ans = []
    for i in range(n):
        if len(tup) > i:
            ans.append(tup[i])

    return ans

def find_dnas_with_longest_span_match_for_pattern(pat, dna_list):
 
    #find the longest DNA for every list
    tup = []
    name_of_DNA_LIST_tup = []
    top_matches_tup = []
    for i in range(len(dna_list)):
        name_of_DNA_LIST_tup.append(dna_list[i][0])
        top_matches_tup.append(top_n_pat_matches_by_span(pat, dna_list[i][1], 1))

    for i in range(len(dna_list)):
        tempTup = []
        tempTup.append(name_of_DNA_LIST_tup[i])
        
        if top_matches_tup[i] == []:
            tempTup.append([])
        else:
            tempTup.append(top_matches_tup[i][0][0])
            tempTup.append(top_matches_tup[i][0][1])
        tup.append(tempTup)
        
    tup.sort(key=lambda x: len(x[1]), reverse=True)
    
    return tup[0]

def count_pattern_matches(pat, dna_list):
    ## your code
    #find all matches
    tup = []
    count = 0
    for i in range(len(dna_list)):
        tempTup = []
        tempTup = find_pat_matches(pat,dna_list[i][1])
        count += len(tempTup)
    
    return count








