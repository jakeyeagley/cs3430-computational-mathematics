#######################
# find_pat_mats.py
# Your NAME
# Your A-NUMBER
#######################

# your code here

import sys
import re

def find_pat_matches(pat, txt):
    ## your code
    ans = []

    it = re.finditer(pat, txt)

    for i in it:
        element = []
        span = []
        
        element.append(i.group(0))
        span.append(i.span())

        element.append(span[0][0])
        element.append(span[0][1])
        ans.append(element)
        
    return ans

pat = sys.argv[1]
txt = sys.stdin.read()

tup = find_pat_matches(pat,txt)

for x in tup:
    sys.stdout.write(str(x[0]))
    sys.stdout.write("\t")
    sys.stdout.write(str(x[1]))
    sys.stdout.write("\t")
    sys.stdout.write(str(x[2]))
    sys.stdout.write("\n")
    


