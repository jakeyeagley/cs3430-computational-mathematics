#######################
# sort_pat_mats.py
# Your NAME
# Your A-NUMBER
#######################

# your code here

import re,sys

order = sys.argv[1]
words = []

if(order == 'dsc'):
    for n in sorted([line for line in sys.stdin.readlines()], reverse=True):
        sys.stdout.write(n)

elif(order == 'asc'):
    for n in sorted([line for line in sys.stdin.readlines()], reverse=False):
        sys.stdout.write(n)

else:
    sys.stdout.write("expecting asc or dsc as an argument")
