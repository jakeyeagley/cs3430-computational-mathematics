#!/usr/bin/python

#################################
# module: py2_dnas_analysis.py
# Jake Yeagley
# A01503588
#################################

import re
import sys
import os
import fnmatch
import math

def find_pat_matches(pat, txt):
    ans = []

    it = re.finditer(pat, txt)

    for i in it:
        element = []
        
        element.append(i.group(0))
        element.append(i.span())

        ans.append(element)
  
    return ans

def sort_pat_matches_by_span(pat, txt, reverse=True):
    tup = find_pat_matches(pat,txt)

    if reverse:
        tup.sort(key=lambda x: len(x[0]))
        tup.reverse()

    else:
        tup.sort(key=lambda x: len(x[0]))
    
    return tup

def top_n_pat_matches_by_span(pat, txt, n):
    tup = sort_pat_matches_by_span(pat, txt, reverse=True)

    ans = []
    for i in range(n):
        if len(tup) > i:
            ans.append(tup[i])

    return ans

def find_dnas_with_longest_span_match_for_pattern(pat, dna_list):

    if(dna_list == []):
      return []

    #find the longest DNA for every list
    tup = []
    name_of_DNA_LIST_tup = []
    top_matches_tup = []
    for i in range(len(dna_list)):
        name_of_DNA_LIST_tup.append(dna_list[i][0])
        top_matches_tup.append(top_n_pat_matches_by_span(pat, dna_list[i][1], 1))

    for i in range(len(dna_list)):
        tempTup = []
        tempTup.append(name_of_DNA_LIST_tup[i])
        
        if top_matches_tup[i] == []:
            tempTup.append([])
        else:
            tempTup.append(top_matches_tup[i][0][0])
            tempTup.append(top_matches_tup[i][0][1])
        tup.append(tempTup)
        
    tup.sort(key=lambda x: len(x[1]), reverse=True)
    
    return tup[0]

def find_dnas_with_shortest_span_match_for_pattern(pat, dna_list):

    if(dna_list == []):
      return []
    #find the longest DNA for every list
    tup = []
    name_of_DNA_LIST_tup = []
    top_matches_tup = []
    for i in range(len(dna_list)):
        name_of_DNA_LIST_tup.append(dna_list[i][0])
        top_matches_tup.append(top_n_pat_matches_by_span(pat, dna_list[i][1], 1))

    for i in range(len(dna_list)):
        tempTup = []
        tempTup.append(name_of_DNA_LIST_tup[i])
        
        if top_matches_tup[i] == []:
            tempTup.append([])
        else:
            tempTup.append(top_matches_tup[i][0][0])
            tempTup.append(top_matches_tup[i][0][1])

        tup.append(tempTup)
        
    tup.sort(key=lambda x: len(x[1]), reverse=False)

    for n in tup:
      if (n[1] == []):
        pass
      else:
        return n

    return []
  
## --------- GENERATE_FILE_NAMES

def generate_file_names(fnpat, rootdir):

  file_list = []
  x = 0
  for path, dirlist, filelist in os.walk(rootdir):
    for file_name in fnmatch.filter(filelist, fnpat):
      file_list.append(os.path.join(path, file_name))
      
  return file_list

def unit_test_01(fnpat, rootdir):
  for fn in generate_file_names(fnpat, rootdir):
    sys.stdout.write(fn + '\n')
  sys.stdout.flush()
        
## ----------- GENERATE_INPUT_STREAMS & GENERATE_LINES
      
def generate_input_streams(gen_filenames):
  tup = []
  count = 0

  for x in gen_filenames:
    filePath = open(x,'r')
    temp = []
    temp.append(x)
    temp.append(filePath)
    tup.append(temp)

  return tup

def generate_dna_strings(gen_instreams):
  tup = []
  for stream in gen_instreams:
    temp = []
    temp.append(stream[0])
    x = ""
    for n in sorted([line for line in stream[1].readlines()]):
      x = x + n
    temp.append(x)
    tup.append(temp)

  return tup

def unit_test_02(fnpat, rootdir):
  dna_fns  = generate_file_names(fnpat, rootdir)
  dna_ins  = generate_input_streams(dna_fns)
  dna_strs = generate_dna_strings(dna_ins)

  for fn, ds in dna_strs:
    sys.stdout.write(fn + '\t' + ds + '\t' + '\n')
  sys.stdout.flush()

def generate_pat_matches(pat, gen_dna_strings):
  tup = []
  for n in gen_dna_strings:
    temp = []
    temp1 = []
    tempPM = []
    temp1.append(n[0])
    tempPM = find_pat_matches(pat, n[1])

    if(tempPM != []):
      temp.append(tempPM[0][0])
      temp.append(tempPM[0][1])
    else:
      temp.append([])
    temp1.append(temp)
    tup.append(temp1)

  return tup

def unit_test_03(fnpat, repat, rootdir):
  dna_fns  = generate_file_names(fnpat, rootdir)
  dna_ins  = generate_input_streams(dna_fns)
  dna_strs = generate_dna_strings(dna_ins)
  pat_mats = generate_pat_matches(repat, dna_strs)

  for fn, pm in pat_mats:
    sys.stdout.write(fn + '\t' + str(pm) + '\n')
  sys.stdout.flush()

def find_max_pat_match(fnpat, repat, rootdir):
  fileNames = []
  fileNames = generate_file_names(fnpat, rootdir)
  inputStreams = []
  inputStreams = generate_input_streams(fileNames)
  dnaStrings = []
  dnaStrings = generate_dna_strings(inputStreams)
  
  longest = ""
  longest = find_dnas_with_longest_span_match_for_pattern(repat, dnaStrings)
  return longest

def find_min_pat_match(fnpat, repat, rootdir):
  fileNames = []
  fileNames = generate_file_names(fnpat, rootdir)
  inputStreams = []
  inputStreams = generate_input_streams(fileNames)
  dnaStrings = []
  dnaStrings = generate_dna_strings(inputStreams)

  shortest = ""
  shortest = find_dnas_with_shortest_span_match_for_pattern(repat, dnaStrings)
  return shortest

def unit_test_04(fnpat, repat, rootdir):
  max_match = find_max_pat_match(fnpat, repat, rootdir)
  min_match = find_min_pat_match(fnpat, repat, rootdir)
  sys.stdout.write(repr(max_match)+'\n')
  sys.stdout.write(repr(min_match)+'\n')
  
if __name__ == '__main__':
  #unit_test_01(sys.argv[1],sys.argv[2])
  #unit_test_02(sys.argv[1],sys.argv[2])
  #unit_test_03(sys.argv[1],sys.argv[2],sys.argv[3])
  unit_test_04(sys.argv[1], sys.argv[2], sys.argv[3])
  pass














